const {google} = require("googleapis");
const path = require("path");
// const {getCredentials} = require("./GoogleAuthenticator");

// Requiring json file is not working here, as google.auth.getClient accepts file path
// let keyFile = require("./google-service-account-credentials.json");
let keyFile = path.join(__dirname, "google-service-account-credentials.json");
// console.log(keyFile);
let scopes = [
    'https://www.googleapis.com/auth/spreadsheets',
     'https://www.googleapis.com/auth/drive',
     'https://www.googleapis.com/auth/calendar',
     "https://www.googleapis.com/auth/contacts",
     "https://www.googleapis.com/auth/plus.login"
 ];

const run = async () => {
    // const ga = await getCredentials();
    const {people} = await google.people({
        version: "v1",
        auth: await google.auth.getClient({
            keyFile,
            scopes
        })
    });

    // const response = await people.connections.list({
    //     resourceName: "people/me",
    //     personFields: "names"
    // });
    const response = await people.get({
        resourceName: "people/me",
        personFields: "names"
    });
    
    console.log(response);
};
run();
