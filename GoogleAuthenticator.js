let {google} = require('googleapis');
let privatekey = require("./google-service-account-credentials.json");
const getJWTAuthClient = () => {
    let jwtAuthClient = new google.auth.JWT(
        privatekey.client_email,
        null,
        privatekey.private_key,
        [
            'https://www.googleapis.com/auth/spreadsheets',
             'https://www.googleapis.com/auth/drive',
             'https://www.googleapis.com/auth/calendar',
             "https://www.googleapis.com/auth/contacts"
         ]
    );
    return jwtAuthClient;
};

const getCredentials = async () => {
    const jwtAuthClient = getJWTAuthClient();
    const credentials = jwtAuthClient.authorize();
    return credentials;
};

module.exports.getJWTAuthClient = getJWTAuthClient;
module.exports.getCredentials = getCredentials;
